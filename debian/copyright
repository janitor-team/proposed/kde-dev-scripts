Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: KDE Development Scripts
Upstream-Contact: Jeremy Paul Whiting <jpwhiting@kde.org>
                  Michael Pyne <mpyne@kde.org>
Source: https://invent.kde.org/sdk/kde-dev-scripts

Files: *
Copyright: 2014, Alex Merry <alex.merry@kde.org>
           2005-2009, Alexandre Julliard <julliard@winehq.org>
           2006-2013, Allen Winter <winter@kde.org>
           2007, Angelo Naselli <anaselli@linux.it> (command line parameters)
           2012-2013, Bruno George Moraes <brunogm0@gmail.com>
           2011, David Faure <faure@kde.org>
           Dirk Mueller
           2013, Dominik Seichter <domseichter@googlemail.com>
           2003, Dominique Devriese <devriese@kde.org>
           2004, Frans Englich <frans.englich@telia.com>
           2003, KDE Development team
           Klaralvdalens Datakonsult AB
           2005-2010, Laurent Montel <montel@kde.org>
           2002, Matt Armstrong
           2005, Mark Kretschmann <markey@web.de>
           2007, Mohamed Hendawi
           2004, Richard Evans <rich@ridas.com>
           2006-2010, Thomas Zander <zander@kde.org>
           2009, Tom Albers <toma@kde.org>
           2006-2008, Tom Albers <tomalbers@kde.nl>
           2005-2006, Tobias Hunger
           2010, Trever Fischer <tdfischer@fedoraproject.org>
           2008, Urs Wolfer <uwolfer@kde.org>
           2003, XEmacs developers
           2002-2003, Zack Rusin <zack@kde.org>
License: GPL-2+

Files: add_trace.pl
       cmake-dependencies.py
       kde-project-info
       reviewboard-am
Copyright: 2014, Aleix Pol Gonzalez <aleixpol@kde.org>
           2013, Aurélien Gâteau <agateau@kde.org>
           2000-2002, David Faure <faure@kde.org>
           2006, Laurent Montel <montel@kde.org>
           2014, Michael Pyne <mpyne@kde.org>
           2005-2008, Thorsten Staerk <kde@staerk.de>
License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: cmake-utils/scripts/generate_findpackage_file
       grantlee_strings_extractor.py
       svn2log.py
Copyright: 2006, Alexander Neundorf <neundorf@kde.org>
           Django Software Foundation and individual contributors
           2008, Laurent Montel <montel@kde.org>
           2010-2011, Stephen Kelly <steveire@gmail.com>
           2003, The University of Wroclaw
License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 3. Neither the name of Django nor the names of its contributors may be used
    to endorse or promote products derived from this software without
    specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: cmake-utils/samples/kcalc/knumber/tests/CMakeLists.txt
       fixfsfaddr.sed
       kf5/convert-kdebug-with-argument.sh
Copyright: 1996-1997, Matthias Kalle Dalheimer <kalle@kde.org>
           2005, Nicolas GOUTTE <goutte@kde.org>
           1997-1998, Stephan Kulow <coolo@kde.org>
License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public License
 along with this library; see the file COPYING.LIB.  If not, write to
 the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 --
 On Debian systems, the complete text of the GNU Library General Public
 License version 2 can be found in `/usr/share/common-licenses/LGPL-2’.

Files: colorcvs
       colorsvn
       fixuifiles
       svnlastchange
       vimdiff-svn
Copyright: 2004, David Faure <faure@kde.org>
           2001-2003, Dirk Mueller <mueller@kde.org>
           1999, Jamie Moyers <jmoyers@geeks.com>
           2007, Matthias Kretz <kretz@kde.org>
           2002, Neil Stevens <neil@qualityassistant.com>
           2005, Thiago Macieira <thiago@kde.org>
License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License as published
 by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 --
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in `/usr/share/common-licenses/GPL-2’.

Files: cvsgettags
       kDebug2kdDebug.sh
       zonetab2pot.py
Copyright: 2000, David Faure <faure@kde.org>
           2002, Lukas Tinkl <lukas@kde.org>
           2004, Roberto Teixeira <roberto@kde.org>
License: GPL-1
 This file is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY.  No author or distributor accepts responsibility
 to anyone for the consequences of using it or for whether it serves any
 particular purpose or works at all, unless he says so in writing.  Refer
 to the GNU General Public License for full details.
 .
 Everyone is granted permission to copy, modify and redistribute this
 file, but only under the conditions described in the GNU General
 Public License.  A copy of this license is supposed to have been given
 to you along with this file so you can know your rights and
 responsibilities.  It should be in a file named COPYING.  Among other
 things, the copyright notice and this notice must be preserved on all
 copies.
 --
 On Debian systems, the full text of the GNU General Public License
 version 1 can be found in the file `/usr/share/common-licenses/GPL-1’.

Files: extend_dmalloc
Copyright: 1995, Gray Watson <gray.watson@letters.com>
License: NTP
 Permission to use, copy, modify, and distribute this software for
 any purpose and without fee is hereby granted, provided that the
 above copyright notice and this permission notice appear in all
 copies, and that the name of Gray Watson not be used in advertising
 or publicity pertaining to distribution of the document or software
 without specific, written prior permission.
 .
 Gray Watson makes no representations about the suitability of the
 software described herein for any purpose.  It is provided "as is"
 without express or implied warranty.

Files: extractattr
       kde-spellcheck.pl
       rename_source_files
Copyright: David Faure <faure@kde.org>
           2004, Richard Evans <rich@ridas.com>
License: LGPL-2
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 --
 On Debian systems, the complete text of the GNU Library General Public
 License version 2 can be found in `/usr/share/common-licenses/LGPL-2’.

Files: generate_string_table.pl
       qt4/adapt-to-icon-spec.py
       rc2kcfgxt.pl
       svnchangesince
Copyright: no copyright claimed
License: public-domain
 No copyright restrictions.

Files: gdb/*
Copyright: 2014, Alex Merry <alex.merry@kde.org>
License: CMU
 Permission to use, copy, modify, and distribute this software
 and its documentation for any purpose and without fee is hereby
 granted, provided that the above copyright notice appear in all
 copies and that both that the copyright notice and this
 permission notice and warranty disclaimer appear in supporting
 documentation, and that the name of the author not be used in
 advertising or publicity pertaining to distribution of the
 software without specific, written prior permission.
 .
 The author disclaims all warranties with regard to this
 software, including all implied warranties of merchantability
 and fitness.  In no event shall the author be liable for any
 special, indirect or consequential damages or any damages
 whatsoever resulting from loss of use, data or profits, whether
 in an action of contract, negligence or other tortious action,
 arising out of or in connection with the use or performance of
 this software.

Files: kde-emacs/kde-emacs-bindings.el
       kde-emacs/kde-emacs-core.el
       kde-emacs/kde-emacs-doc.el
       kde-emacs/kde-emacs-general.el
       kde-emacs/kde-emacs-semantic.el
       kde-emacs/kde-emacs-utils.el
       kde-emacs/kde-emacs.el
Copyright: 2002-2005, KDE Development Team <www.kde.org>
           2002, Zack Rusin <zack@kde.org>
           2002, Zack Rusin <zackrat@att.net>
License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301  USA
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2.1 can be found in "/usr/share/common-licenses/LGPL-2.1".

Files: kde-emacs/fdl.texi
       kde-emacs/kde-emacs-tips.texi
Copyright: 2000-2008, Free Software Foundation, Inc
           2002, Zack Rusin & KDE Development Team
License: GFDL-1.3
 Permission is granted to copy, distribute and/or modify this document
 under the terms of the GNU Free Documentation License, Version 1.3
 or any later version published by the Free Software Foundation;
 with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
 A copy of the license is included in the section entitled.
 --
 On Debian systems, the complete text of the GNU Free Documentation License
 version 1.3 can be found in `/usr/share/common-licenses/GFDL-1.3’

Files: pim-build-deps-graphs.py
Copyright: 2017, Sandro Knauß <sknauss@kde.org>
License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 --
 On Debian systems, the full text of the GNU General Public License
 version 3 can be found in the file `/usr/share/common-licenses/GPL-3’.

Files: svnintegrate
Copyright: 2006, Thiago Macieira <thiago@kde.org>
License: Artistic-2.1
 On Debian systems, the complete text of the "Artistic License" can be
 found in "/usr/share/common-licenses/Artistic"

Files: debian/*
Copyright: 2007-2015, Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 --
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in `/usr/share/common-licenses/GPL-2’.
